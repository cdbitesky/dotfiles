base_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

command_name="vim"

command_gate () {
  if [ "$#" -ne 2 ]; then
    echo "Usage: command_gate <command> <callback>"
    exit 1
  fi
  command -v "$1" >/dev/null 2>&1 &&
      echo "Found $1 at `which $1`. Setting up configuration" &&
      $2 || {
        echo "I require $1 but it's not installed." >&2;
        exit 1;
      }
}

action () {
    if [ -d $HOME/.vim ]; then
        echo "'~/.vim' already exists";
        exit -1;
    fi

    if [ -f $HOME/.vimrc ]; then
        echo "'~/.vimrc' already exists";
        exit -1;
    fi

    ln -s "$base_path" ~/.vim
    ln -s "$base_path/vimrc" ~/.vimrc
}

command_gate "$command_name" action

