# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Theme location ~/.oh-my-zsh/themes/
ZSH_THEME="random" # Pick a random Theme
#ZSH_THEME="adben" # Default Theme


## oh-my-zsh options

  # Set to this to use case-sensitive completion
  # CASE_SENSITIVE="true"

  # Comment this out to disable bi-weekly auto-update checks
  # DISABLE_AUTO_UPDATE="true"

  # Uncomment to change how many often would you like to wait before auto-updates occur? (in days)
  export UPDATE_ZSH_DAYS=30

  # Uncomment following line if you want to disable colors in ls
  # DISABLE_LS_COLORS="true"

  # Uncomment following line if you want to disable autosetting terminal title.
  # DISABLE_AUTO_TITLE="true"

  # Uncomment following line if you want red dots to be displayed while waiting for completion
  # COMPLETION_WAITING_DOTS="true"

  # Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
  # Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
  # Example format: plugins=(rails git textmate ruby lighthouse)
  plugins=(git)

  source $ZSH/oh-my-zsh.sh

## exports

  # Homebrew
  # Note: Homebrew linkage is intentionally first to preload /usr/bin
  export PATH="/opt/homebrew/bin:$PATH"
  export PATH="/opt/homebrew/sbin:$PATH"

  # Default bins
  export PATH=$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:$HOME/opt/bin:$HOME/opt/bin;

  ## Go
    export GOROOT=$HOME/go
    export GOARCH=386
    export GOOS=linux
    export GOBIN=$HOME/gobin

    # Export go bin
    export PATH=$PATH:$GOBIN

  # latex
  export PATH=$PATH:/usr/texbin;

  # Ruby on Rails
  #export PATH=$PATH:$HOME/.rvm/bin;
  #export PATH=$PATH:$HOME/.rvm/gems/ruby-*/bin;
  #[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
  #[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm" # Load RVM function

  # Vim syntax_checkers
  export PATH=$PATH:$HOME/opt/syntax_checkers;

  ### Added by the Heroku Toolbelt
  export PATH="/usr/local/heroku/bin:$PATH"

  # NVM
  export NVM_DIR="$HOME/.nvm"
  [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

## Limits
  ulimit -n 10000

## aliases

  # apt
  if which apt-get > /dev/null; then
    alias update='sudo apt-get update';
    alias upgrade='sudo apt-get upgrade';
    alias install='sudo apt-get install';
  fi;
  if which apt-cache > /dev/null; then
    alias search='apt-cache search'
  fi;

  # color
  if [[ "$OSTYPE" == "darwin"* ]]; then
    alias ls='ls -G'
  else
    alias ls='ls --color'
  fi
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'

  # tree
  if which tree > /dev/null; then
    alias tree='tree -CD -L 2';
  fi

  # operations
  alias size='du -hs *'
  alias makagiga='java -jar $HOME/opt/makagiga-3.8.14/makagiga.jar'

  # l
  alias ll='ls -alF'
  alias la='ls -A'
  alias l='ls -lFh'
  alias la='l -A'

  # vim
  alias v='vim'

  # cd
  alias .='cd ../'
  alias ..='cd ../../'
  alias ...='cd ../../../'

  # Z no correct
  alias tor='nocorrect tor '
  alias t='nocorrect t'

  # git
  alias gs='git status -vs'
  alias ga='git add'
  alias gco='git checkout'

## Utility scripts
  # Trim trailing whitespace
  function trim {
    find . -name $1 -type f -print0 | xargs -0 sed -i '' -E "s/[[:space:]]*$//"
  }

  # SSH Tunnel
  function tunnel() {
    local portNumber=${1};
    [[ ! -z $portNumber ]] || (echo "Missing port number" && return -1);
    local host=${2};
    [[ ! -z $host ]] || (echo "Missing a host to tunnel to" && return -1);

    echo "Tunneling to $host:${portNumber}";
    ssh -o ProxyCommand=none -T -N -L "${portNumber}:localhost:${portNumber}" $host;
  }

## Colors
  # Colors for file nav
  LS_COLORS='ow=01;36;40'; export LS_COLORS
